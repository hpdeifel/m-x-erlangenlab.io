{ nixpkgs ? import <nixos> {}, compiler ? "ghc865" }:

{
  site = nixpkgs.haskellPackages.callPackage ./site.nix {};
}
