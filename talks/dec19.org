---
title: M-x Dezember - Show and Tell and Lebkuchen and Punch
date: 2019-12-17
location: Cauerstraße 11
room: 00.131
time: 18:30
description: A friendly get together with food and drinks to end the year
---
Let's have a relaxed show & tell where everybody can show nice things they want to show and discuss interesting topics about our favourite LISP interpreter.

This will be accompanied by hot punch and Lebkuchen.

I (Merlin) recently read the AucTeX manual start to finish so I can show my Emacs LaTeX setup if people are interested.

If you know what you want to show already you can comment below.

*Update:* We learned about AucTeX and the Emacs calculator, ending with a drink and snack at the Emacs fireplace.
