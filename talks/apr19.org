---
title: Language Server Protocol and Emacs
author: Hans-Peter
date: 2019-04-25
location: Cauerstraße 11
room: 00.131
time: 18:30
description: An introduction on how to use language servers in Emacs
---
We discussed last time that we want to explore the integration of servers communicating via the Language Server Protocol in Emacs. This presents a unified way of enabling IDE-like features in our favourite text editor.

This meetup is going to be a kind-of hands-on session to get LSP integration running and solving any issues that may arise when doing that.
