---
title: Haskell Development in Emacs 
date: 2020-07-30
location: DFNconf
time: 18:30
description: We learn about the different options of setting up Emacs as a Haskell IDE
author: Merlin
conf-link: https://conf.dfn.de/webapp/conference/979117107
---
* haskell-mode
  - basic haskell integration
  - only static integration
  - knows basic library functions
** interactive-haskell-mode
   - makes use of running ghci session
   - gets all signatures from ghci
   - parses ghci output
** nix-haskell-mode
   - adapter for running ghci in nix-shell
   - needs annoying reload ritual when dependencies change
** emamux-ghci
   - stale since 2015
   - can run ghci session inside of tmux
* ghc-mod
  - officially dead
  - needs ghc-mod executable
  - had hole-based programming before ghci
  - had case splitting
  - was used and then replaced by [[hie]]
* ghcid
  - running ghci session
  - filesystem watcher to auto reload
  - can specify ghci commands to run after successfull reload
  - can evaluate expressions in comments
* intero
  - officially dead
  - very easy setup
  - utilized flycheck for linting
  - integrated with eldoc
  - had issues with nix 
  - limited to stack projects
** dante
   - fork of [[intero]]
   - not limited to stack
   - frontend for ghci
   - has nix support
   - couldn't get it to work
   - eval code in comments
* HaRe
  - stale since 2017
  - refactoring tool
  - doesn't work with ghc 8
* structured-haskell-mode
  - stale since 2018
  - could not get it to work with current haskell mode
  - paredit-style strutured editing
  - case splitting for simple sum types
  - [[https://github.com/projectional-haskell/structured-haskell-mode/blob/master/README.md][Features]]
* lsp-haskell
** haskell-ide-engine - hie <<hie>>
   - official haskell LSP server
   - has been around before LSP was a thing
   - dev effort moved to [[haskell-language-server]]
   - continued monthly releases
** ghcide
   - develloped by digital asset
   - used in emacs as [[hie]] replacement
** haskell-language-server - hls <<hls>>
   - merger of [[hie]] and [[ghcide]]
   - currently not as feature complete as [[hie]]
   - advertises more features than ghcide
   - although some I didn't get to work
