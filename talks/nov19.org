---
title: Introduction to Emacs Major Modes
author: Philip
date: 2019-11-21
location: Cauerstraße 11
room: 00.131
time: 18:30
description: How to write Emacs major modes
---
** Want to write your own Emacs major modes? But don't know where and how to start? 
Awesome! Philip will give a short introduction to get you started.
